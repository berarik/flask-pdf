from flask import Flask, render_template
from pdf import create_pdf


app = Flask(__name__)


@app.route('/')
def v():
    return render_template('index.html', number=10)


@app.route('/index')
def ff():
    pdf = create_pdf(render_template('index.html', number=10))
    pdf_out = pdf.getvalue()
    f = open('pdf.pdf', 'w')
    f.write(pdf_out)
    f.close()
    return render_template('index.html', number=10)
app.run()
